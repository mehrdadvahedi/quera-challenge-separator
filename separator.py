def separator(ls):
    ls_1 = []
    ls_2 = []
    for item in ls:
        if item % 2 == 0:
            ls_1.append(item)
        else:
            ls_2.append(item)
    ans = (ls_1, ls_2)
    return ans